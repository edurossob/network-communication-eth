#include "package.h"
#include <sys/time.h>

int soquete;

package_queue_t* packs;

int create_user(char** username){
  unsigned char *msg;
  size_t len = 0;
  ssize_t nameSize = 0;

  do{
    printf("Insira seu nome de usuário:\n");
    nameSize = getline(username, &len, stdin);

    if(nameSize < 2) printf("Seu nome precisa ter no mínimo uma letra!\n");
  } while (nameSize < 2);

  (*username)[nameSize-1] = '\0';

  return 1;
}

int cria_raw_socket(char *device)
{
  int soquete;
  struct ifreq ir;
  struct sockaddr_ll endereco;
  struct packet_mreq mr;

  soquete = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));  	/*cria socket*/
  if (soquete == -1) {
    fprintf(stderr, "Erro no socket. Verifique se você é sudo! (%d)\n", errno);
    exit(-1);
  }

  memset(&ir, 0, sizeof(struct ifreq));  	/*dispositivo eth0*/
  memcpy(ir.ifr_name, device, sizeof(device));
  if (ioctl(soquete, SIOCGIFINDEX, &ir) == -1) {
    fprintf(stderr, "Erro no ioctl\n");
    exit(-1);
  }
	

  memset(&endereco, 0, sizeof(endereco)); 	/*IP do dispositivo*/
  endereco.sll_family = AF_PACKET;
  endereco.sll_protocol = htons(ETH_P_ALL);
  endereco.sll_ifindex = ir.ifr_ifindex;
  if (bind(soquete, (struct sockaddr *)&endereco, sizeof(endereco)) == -1) {
    fprintf(stderr, "Erro no bind\n");
    exit(-1);
  }


  memset(&mr, 0, sizeof(mr));          /*Modo Promiscuo*/
  mr.mr_ifindex = ir.ifr_ifindex;
  mr.mr_type = PACKET_MR_PROMISC;
  if (setsockopt(soquete, SOL_PACKET, PACKET_ADD_MEMBERSHIP, &mr, sizeof(mr)) == -1)	{
    fprintf(stderr, "Erro ao fazer setsockopt\n");
    exit(-1);
  }

  return soquete;
}

int send_package(package_t* pack){
    unsigned char *msg;
    
    msg = package_to_char(pack);

    fprintf(stderr, "Tentando enviarr.... ");

    int r = send(soquete, msg, pack->head->size + 4, 0); 
    if (r == -1) {
      printf("Erro no send %d\n", errno);
      exit(-1);
    }
    fprintf(stderr, "enviado!\n");

    fprintf(stderr, "%d\n", r);
    free(msg);
}


int main(int argc, char const *argv[])
{
  soquete = cria_raw_socket("lo");
  fprintf(stderr, "Socket %d created successfully\n", soquete);

  char *line;
  size_t len = 0;
  ssize_t lineSize = 0;

  package_t* pack;
  

  char* username;
  create_user(&username);

  while(1){

    printf("Escreva algo, %s!: ", username);

    lineSize = getline(&line, &len, stdin) - 1;
    line[lineSize] = '\0';

    line_to_package(line, lineSize, &packs);
    // queue_size((queue_t*) &packs);

    while(packs != NULL){
      package_queue_t * elem = packs;
      send_package(elem->pack);
      queue_remove((queue_t**) &packs, (queue_t*) elem);
      sleep(1);
    }
  }

  free(line);

  return 0;
}


/*

get username
create raw socket

read message
  (if message is less than 14 char, put mask on it)
put message in queue

get message from queue
send to socket
wait for response
if nack: send again

*/