#ifndef __PACKAGE__
  #define __PACKAGE__
  #include <sys/types.h>
  #include <sys/socket.h>
  #include <sys/ioctl.h>
  #include <net/ethernet.h>
  #include <linux/if_packet.h>
  #include <linux/if.h>
  #include <stdlib.h>
  #include <string.h>
  #include <stdio.h>
  #include <arpa/inet.h>
  #include <errno.h>
  #include <netinet/ip.h>

  #include "queue.h"

  #define INIT_MARK       0x7e

  #define TEXT_TYPE       0x01
  #define MIDIA_TYPE      0x10
  #define ACK_TYPE        0x0A 
  #define NACK_TYPE       0x00 
  #define ERROR_TYPE      0x1E  
  #define INI_TRANS_TYPE  0x1D  
  #define END_TRANS_TYPE  0x0F  
  #define DATA_TYPE       0x0D

  #define MIN_MSG_SIZE 14 // Mensagem deve ser igual ou maior que 14

  struct header_t {
    unsigned int init_mark : 8;
    unsigned int type : 6;
    unsigned int sequence : 4;
    unsigned int size : 6;
  } typedef header; 

  #define HEADER_SIZE 3

  struct package_t {
    header * head;
    char ** data;
    uint8_t crc;
  } typedef package_t;

  typedef struct package_queue_t {
    struct package_queue_t *prev ;  // ptr para usar cast com queue_t
    struct package_queue_t *next ;  // ptr para usar cast com queue_t
    package_t *pack ;
    // outros campos podem ser acrescidos aqui
  } package_queue_t ;

  #define MAX_SIZE_DATA 63

  /*
  * message = header (3) + data (0 ~ 63) + crc (1) = 4 ~ 67 bytes
  */

  uint8_t crc8(char* data, int len);
  void print_elem_pack(package_queue_t* elem);
  void print_pack(package_t* pack);
  void print_data(package_t* pack);
  unsigned char * package_to_char(package_t * pack);
  package_t* create_package(char* line, int lineSize, int type);
  package_t * line_to_package(char* line, int lineSize, package_queue_t** packs);
  int file_to_package(FILE* fp, package_queue_t** packs);
  package_queue_t* char_to_package(char *msg, int size);
  int compare_packs_by_seq(package_queue_t* a, package_queue_t* b);
  int compare_packs(package_queue_t* pa, package_queue_t* pb);


#endif