#include "package.h"

unsigned int sequence = 0;

uint8_t crc8(char* data, int len)
{

  if (len == 0)
    return 0;


  uint8_t crc = 0xff;
  uint8_t gen = 0x31;
  size_t i, j;
  for (i = 0; i < len; i++) {
    crc ^= data[i];
    for (j = 0; j < 8; j++) {
      if ((crc & 0x80) != 0)
        crc = (uint8_t)((crc << 1) ^ gen);
      else
        crc <<= 1;
    }
  }
  return crc;
}

void print_elem_pack(package_queue_t* elem){
  package_t* pack = elem->pack;
  print_pack(pack);
}

void print_pack(package_t* pack){
  printf ("-> |0x%x|0x%06x|0x%04x|0x%04x|%s|0x%0d|\n", 
    pack->head->init_mark,
    pack->head->sequence,
    pack->head->type,
    pack->head->size,
    *pack->data,
    pack->crc
  );
}

void print_data(package_t* pack){
  long time = 123456789;
  printf("[%ld] %s: %s\n", time, "Edu", *pack->data);
}

int compare_packs_by_seq(package_queue_t* a, package_queue_t* b){
  fprintf(stderr, "Comparando elementos por numero de sequencia : %d e %d\n", a->pack->head->sequence, b->pack->head->sequence);

  if(a->pack->head->type == ACK_TYPE && a->pack->head->type == NACK_TYPE
      && b->pack->head->type == ACK_TYPE && b->pack->head->type == NACK_TYPE)
    return 0;

  if(a == NULL || b == NULL)
    return 0;
  if(a->pack->head->init_mark != b->pack->head->init_mark)
    return 0;
  if(a->pack->head->sequence != b->pack->head->sequence)
    return 0;
  fprintf(stderr, "Elementos iguais!\n");

  return 1;
}

int compare_packs(package_queue_t* a, package_queue_t* b){
  fprintf(stderr, "Comparando elementos\n");

  if(a == NULL || b == NULL)
    return 0;
  fprintf(stderr, "INFO: [compare_packs] 0 \n");

  if(a->pack->head->init_mark != b->pack->head->init_mark)
    return 0;
  fprintf(stderr, "INFO: [compare_packs] 1 \n");
  if(a->pack->head->sequence != b->pack->head->sequence)
    return 0;
  fprintf(stderr, "INFO: [compare_packs] 2 \n");
  if(a->pack->head->size != b->pack->head->size)
    return 0;
  fprintf(stderr, "INFO: [compare_packs] 3 \n");
  if(a->pack->head->type != b->pack->head->type)
    return 0;
  fprintf(stderr, "INFO: [compare_packs] 4 \n");
  if( strncmp(*a->pack->data, *b->pack->data, a->pack->head->size) )
    return 0;
  fprintf(stderr, "INFO: [compare_packs] 5 \n");
  if(a->pack->crc != b->pack->crc)
    return 0;


  fprintf(stderr, "Elementos iguais!\n");

  return 1;
}


/* Transforma um pacote em uma string para ser impressa no socket */
unsigned char * package_to_char(package_t * pack){
  fprintf( stderr, "INFO: STARTING package_to_char --\n");
  header * head = pack->head;

  // fprintf( stderr, ">. %s, %p\n ToChar->", *pack->data, pack->data[0]);

  // print_pack(pack);

  int msgSize = HEADER_SIZE +  head->size + sizeof(unsigned int);
  // fprintf( stderr, "size %d = %d + %d + %ld \n", msgSize, HEADER_SIZE, head->size, sizeof(unsigned int));

  unsigned char * fullMsg = malloc(msgSize * sizeof(char));

  memcpy(fullMsg, head, HEADER_SIZE);
  // fprintf( stderr, "head %d\n", HEADER_SIZE);


  memcpy(fullMsg + HEADER_SIZE, *pack->data,  head->size);
  // fprintf( stderr, "Data %d\n", HEADER_SIZE+ head->size);
  
  uint8_t crc = crc8(*pack->data, pack->head->size);
  memcpy(fullMsg + HEADER_SIZE+ head->size, &crc, sizeof(unsigned int));

  // fprintf( stderr, "CRC %ld\n", HEADER_SIZE+ head->size+sizeof(unsigned int));

  // for(int i = HEADER_SIZE; i < HEADER_SIZE + pack->head->size; i++){
  //   fprintf( stderr, "%c ", fullMsg[i]);
  // }fprintf( stderr, "{%d}\n", fullMsg[HEADER_SIZE + pack->head->size]);

  fprintf( stderr, "INFO: END package_to_char --\n");

  return fullMsg;
}

package_t* create_package(char* line, int lineSize, int type){
  fprintf(stderr, "INFO: STARTING create_package -- \n");
  header * head = malloc(sizeof(header));
  package_t * pack = malloc(sizeof(package_t));

  int current_seq = sequence;

  if(type == ACK_TYPE || type == NACK_TYPE){
    current_seq = lineSize;
    lineSize = 0;
    sequence --;
  }

  if(lineSize < MIN_MSG_SIZE)
    lineSize = MIN_MSG_SIZE;
  
  if(line == NULL || line == ""){
    char * l = malloc(lineSize * sizeof(char));
    line = l;
  }

  char * s = malloc(lineSize * sizeof(char));
  char ** data =  malloc(sizeof(char*));
  data[0] = s;
  
  strncpy(s, line, lineSize);

  pack->head = head;

  pack->head->init_mark = INIT_MARK;



  pack->head->sequence = current_seq;
  sequence = (sequence+1) % 16;

  pack->head->type = type;
  pack->head->size = lineSize;
  pack->data = data;
  pack->crc = crc8(*pack->data, pack->head->size);




  // print_pack(pack);
  
  fprintf(stderr, "INFO: END create_package -- \n");
  return pack;
}

package_t * line_to_package(char* line, int lineSize, package_queue_t** packs){
  fprintf(stderr, "INFO: [line_to_package] BEGINING -- \n");

  int size_faltando = lineSize;

  char * cutLine = malloc(MAX_SIZE_DATA * sizeof(char));

  
  fprintf(stderr, "INFO: [line_to_package] %d char, so %d packages\n", lineSize, (lineSize/MAX_SIZE_DATA)+1);

  while (size_faltando > MAX_SIZE_DATA) {
    memcpy(cutLine, &line[lineSize-size_faltando], MAX_SIZE_DATA);
    package_t* pack = create_package(cutLine, MAX_SIZE_DATA, TEXT_TYPE);
    size_faltando -= MAX_SIZE_DATA;
    package_queue_t* elem = malloc(sizeof(package_queue_t));

    elem->next = NULL;
    elem->prev = NULL;
    elem->pack = pack;

    queue_append((queue_t**) packs, (queue_t*) elem );
  };

  if(size_faltando < MIN_MSG_SIZE){
    for(int i = 0; i < MIN_MSG_SIZE; i++){
      cutLine[i] = ' ';
    }
  }

  fprintf(stderr, "INFO: [line_to_package] Chegou aqui faltando %d\n", size_faltando);
  
  memcpy(cutLine, &line[lineSize-size_faltando], size_faltando);

  package_t* pack = create_package(cutLine, size_faltando, TEXT_TYPE);

  package_queue_t* elem = malloc(sizeof(package_queue_t));

  elem->next = NULL;
  elem->prev = NULL;
  elem->pack = pack;

  fprintf(stderr, "INFO: [line_to_package] Inserindo último elemento na fila -- \n");
  
  queue_append((queue_t**) packs, (queue_t*) elem );
  // queue_size((queue_t*)*packs);

  fprintf(stderr, "INFO: [line_to_package] Finalizado line_to_pack\n"); 
  return pack;
}


int file_to_package(FILE* fp, package_queue_t** packs){
  fprintf(stderr, "INFO: [file_to_package] BEGINING -- \n");

  char * stream = malloc(MAX_SIZE_DATA * sizeof(char));

  sprintf(stream, "arquivo");  
  int nameSize = strlen(stream);

  package_t* ini = create_package(stream, nameSize, INI_TRANS_TYPE);
  package_queue_t* elem = malloc(sizeof(package_queue_t));

  elem->next = NULL;
  elem->prev = NULL;
  elem->pack = ini;
  queue_append((queue_t**) packs, (queue_t*) elem );
  
  fprintf(stderr, "INFO: [file_to_package] Criado ini_trans \n");

  
  while( fgets ( stream, MAX_SIZE_DATA, fp ) != NULL ){
    package_t* pack = create_package(stream, MAX_SIZE_DATA, MIDIA_TYPE);
    elem = malloc(sizeof(package_queue_t));

    elem->next = NULL;
    elem->prev = NULL;
    elem->pack = pack;

    queue_append((queue_t**) packs, (queue_t*) elem );
    fprintf(stderr, "INFO: [file_to_package] criado conteúdo \n");

  }


  sprintf(stream, "arquivo");  
  package_t* fin = create_package(stream, nameSize, END_TRANS_TYPE);
  elem = malloc(sizeof(package_queue_t));

  elem->next = NULL;
  elem->prev = NULL;
  elem->pack = fin;
  queue_append((queue_t**) packs, (queue_t*) elem );

  fprintf(stderr, "INFO: [file_to_package] Criado fim_trans \n");


  // queue_size((queue_t*)*packs);

  fprintf(stderr, "INFO: [file_to_package] Finalizado file_to_package\n"); 
  return 1;
}


package_queue_t* char_to_package(char *msg, int size){
  fprintf (stderr, "INFO: BEGINING char_to_package -- \n");

  package_t * pack = malloc(sizeof(package_t));
  pack->head = malloc(HEADER_SIZE);

  memcpy(pack->head, msg, HEADER_SIZE);

  fprintf (stderr, "Head: Init[%x], Type[%02x], Seq[%01x], size[%02x]\n", pack->head->init_mark, pack->head->type, pack->head->sequence, pack->head->size );

  int lineSize = pack->head->size;
  
  char * s = malloc(lineSize * sizeof(char));
  char ** data =  malloc(sizeof(char*));
  data[0] = s;
  memcpy(s, msg + HEADER_SIZE, lineSize);
  pack->data = data;

  fprintf (stderr, "Data: '%s'\n", *pack->data);

  pack->crc = msg [size-1];
  fprintf (stderr, "Received CRC: %d\n", pack->crc);


  package_queue_t * p = malloc(sizeof(package_queue_t));
  p->next = NULL;
  p->prev = NULL;
  p->pack = pack;

  fprintf (stderr, "INFO: END char_to_package -- \n\n");
  return p;
}
