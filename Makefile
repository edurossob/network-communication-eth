HARDCORE  = -pipe \
	  -ggdb3 -Wstrict-overflow=5 -fstack-protector-all \
    -W -Wall -Wextra \
	  -Wbad-function-cast \
	  -Wcast-align \
	  -Wcast-qual \
	  -Wconversion \
	  -Wfloat-equal \
	  -Wformat-y2k \
	  -Winit-self \
	  -Winline \
	  -Winvalid-pch \
	  -Wmissing-declarations \
	  -Wmissing-field-initializers \
	  -Wmissing-format-attribute \
	  -Wmissing-include-dirs \
	  -Wmissing-noreturn \
	  -Wmissing-prototypes \
	  -Wnested-externs \
	  -Wnormalized=nfc \
	  -Wold-style-definition \
	  -Woverlength-strings \
	  -Wpacked \
	  -Wpadded \
	  -Wpointer-arith \
	  -Wredundant-decls \
	  -Wshadow \
	  -Wsign-compare \
	  -Wstack-protector \
	  -Wstrict-aliasing=2 \
	  -Wstrict-prototypes \
	  -Wundef \
	  -Wunsafe-loop-optimizations \
	  -Wvolatile-register-var \
	  -Wwrite-strings

OUTFILE = server

LIBS = -lncurses

CC = gcc

#------------------------------------------------------------------------------
.PHONY : all clean

#------------------------------------------------------------------------------
all : server client

server : server.c queue.c queue.h package.c package.h
	$(CC) -o server queue.c package.c server.c $(LIBS)

client : client.c queue.c queue.h package.c package.h
	$(CC) -o client client.c queue.c package.c $(LIBS)

#------------------------------------------------------------------------------
clean :
	$(RM) server client *.o

purge :
	$(RM) server client *.o $(OUTFILE)