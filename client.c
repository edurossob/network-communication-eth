#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/ethernet.h>
#include <linux/if_packet.h>
#include <linux/if.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <fcntl.h>
#include <ncurses.h>

#include "queue.h"
#include "package.h"

int soquete;

package_queue_t* fila_para_enviar;
package_queue_t* fila_enviando;

int tamanho_fila_enviando;
#define STD_TAMANHO_JANELA 12

FILE *file_recieving;

int mode;
#define RECIEVE_MODE 0
#define WRITE_MODE 1
#define FILE_MODE 2
#define RECIEVE_FILE_MODE 3
#define WAITING_ANSWER_MODE 4

#define MAX_SIZE_OF_INPUT 256

typedef struct display_messages_queue_t {
  struct display_messages_queue_t *prev ;  // aponta para o elemento anterior na fila
  struct display_messages_queue_t *next ;  // aponta para o elemento seguinte na fila
  int user_id;
  long time;
  char* txt;
} display_messages_queue_t;

display_messages_queue_t ** fila_historico;

void desenha_tabuleiro(int mode,  char* username){
	int i;

	for(i = 1; i < 122; i++){
		move(0, i);
		addch('_');
		move(5, i);
		addch('_');
		move(37, i);
		addch('_');
	}
	for(i = 1; i < 38; i++){	
		move(i, 0);
		addch('|');
		move(i, 122);
		addch('|');
	}
	for(i = 6; i < 38; i++){	
		move(i, 100);
		addch('|');
  }
	/*     "012345678901234567890" */

/* Header */

  move(2, 3);
  addstr("Usuário:");
  move(2, 13);
  addstr(username);

  move(3, 3);
  addstr("Servidor:");


/* Left Sidebar */
	move(7, 101);
	addstr("  MODO : ");
 	move(7, 110);
  if(mode == RECIEVE_MODE) { // Modo recebimento
  	addstr("Recebimento");
    
    move(25, 101);
    addstr("Digite 'i' para");
    move(26, 101);
    addstr("Inserção");
    
    move(27,101);
    addstr("Digite 'a' para");
    
    move(28, 101);
    addstr("Arquivos");
  } else if (mode == WRITE_MODE) { // Modo inserção
    addstr("Inserção   ");
    move(25, 101);
    addstr("Clique 'enter' para");
    move(26, 101);
    addstr("enviar");
    for(int i = 1; i < 100; i++){
      move(34, i);
      addch('_');
    }
    move(34, 2);
    addstr("Insira seu texto:");

  } else if (mode == FILE_MODE) { // Modo inserção
    addstr("Arquivos   ");
    move(25, 101);
    addstr("Clique 'enter' para");
    move(26, 101);
    addstr("enviar");
    for(int i = 1; i < 100; i++){
      move(34, i);
      addch('_');
    }
    move(34, 2);
    addstr("Escreva o caminho do arquivo a ser enviado:");
  } else if (mode == RECIEVE_FILE_MODE){
    move(34, 2);
    addstr("RECEBENDO ARQUIVO!!");
  } else if (mode == WAITING_ANSWER_MODE){
    move(34, 2);
    addstr("MENSAGEM ENVIADA, ESPERANDO RESPOSTA!!");
  }
	move(33, 101);
	addstr("_____________________");
	move(34, 101);
	addstr("Feito por:");
	move(35, 101);
	addstr("Eduardo Rosso B.");
	move(36, 101);
	addstr("GRR 20190378");
}

void desenha_msgs(display_messages_queue_t  ** fila_chat,  char* username){
  display_messages_queue_t * elem = *fila_chat;
  
  if (*fila_chat == NULL)
    return ;

  int fila_size = queue_size( (queue_t*) *fila_chat);

  if (fila_size > 13) { // Limita o máximo do que pode aparecer no chat
    fila_size = 13;
    for (int i = 0; i < fila_size; i++){
      elem = elem->prev;
    }
  }

  for (int i = 0; i < fila_size*2; i+=2){
    
    move(6+i, 3);
    if(elem->user_id == 0){
      addstr(username);
    } else {
      addstr("Usuário externo:");
    }

    move(7+i, 5);
    addstr(elem->txt);
    elem = elem->next;
  }

}

void desenha_texto(char* txt){
  if(txt[0] != '\0'){
   
    move(35, 3);
    addstr(txt); 
  }
}


void adiciona_no_historico(int id, package_queue_t* pack){
  display_messages_queue_t* message = malloc(sizeof(display_messages_queue_t));

  message->next = NULL;
  message->prev = NULL;
  message->user_id = id;
  message->time = 10;
  if(pack->pack->head->type == END_TRANS_TYPE){
    char* m = "Enviou um arquivo!";
    message->txt = malloc(strlen(m) * sizeof(char));
    sprintf(message->txt, "%s",m);
  }else{
    message->txt = malloc(strlen(*pack->pack->data) * sizeof(char));
    strcpy(message->txt, *pack->pack->data);
  }
  queue_append((queue_t**) fila_historico, (queue_t*) message);

  fprintf(stderr, "INFO: [adiciona_no_historico] Adicionando %s\n", message->txt);
  queue_size((queue_t*)*fila_historico);
}

/* -- Envia pacote e o insere na fila -- */
int send_package(package_t* pack, package_queue_t ** fila){
    unsigned char *msg;
    
    msg = package_to_char(pack);

    fprintf(stderr, "INFO: [send_package] Tentando enviarr.... ");

    int r = send(soquete, msg, pack->head->size + 4, 0);

    if (r == -1) { 
      printf("ERR: [send_package] Erro no send %d\n", errno);
      exit(-1);
    }
    fprintf(stderr, "INFO: [send_package] enviado!\n");


    if(fila != NULL){
      package_queue_t* p = malloc(sizeof(package_queue_t));
      p->next = NULL;
      p->prev = NULL;
      p->pack = pack;

      queue_append((queue_t**) fila, (queue_t*) p);
      fprintf(stderr, "INFO: [send_package] inserido em fila adequada!\n");

    }
    free(msg);
}

void* send_package_print(package_queue_t* pack){
  send_package(pack->pack, NULL);
}

int send_ack(package_queue_t* pack){
  fprintf (stderr, "INFO: BEGINING send_ack -- %d\n", pack->pack->head->sequence);

  package_t* p = create_package(NULL, pack->pack->head->sequence, ACK_TYPE);
  send_package (p, NULL);

  fprintf (stderr, "INFO: END send_ack -- \n");
  return 1;
}

int send_nack(package_queue_t* pack){
  fprintf (stderr, "INFO: BEGINING send_nack -- \n");

  package_t* p = create_package(NULL, pack->pack->head->sequence, NACK_TYPE);
  send_package (p, NULL);

  fprintf (stderr, "INFO: END send_nack -- \n");
  return 1;
}

int cria_raw_socket(char *device)
{
  int soquete;
  struct ifreq ir;
  struct sockaddr_ll endereco;
  struct packet_mreq mr;

  soquete = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));  	/*cria socket*/
  if (soquete == -1) {
    fprintf(stderr, "Erro no Socket\n");
    exit(-1);
  }

  fcntl(soquete, F_SETFL, O_NONBLOCK);

  memset(&ir, 0, sizeof(struct ifreq));  	/*dispositivo eth0*/
  memcpy(ir.ifr_name, device, sizeof(device));
  if (ioctl(soquete, SIOCGIFINDEX, &ir) == -1) {
    fprintf(stderr, "Erro no ioctl\n");
    exit(-1);
  }
	

  memset(&endereco, 0, sizeof(endereco)); 	/*IP do dispositivo*/
  endereco.sll_family = AF_PACKET;
  endereco.sll_protocol = htons(ETH_P_ALL);
  endereco.sll_ifindex = ir.ifr_ifindex;
  if (bind(soquete, (struct sockaddr *)&endereco, sizeof(endereco)) == -1) {
    fprintf(stderr, "Erro no bind\n");
    exit(-1);
  }


  memset(&mr, 0, sizeof(mr));          /*Modo Promiscuo*/
  mr.mr_ifindex = ir.ifr_ifindex;
  mr.mr_type = PACKET_MR_PROMISC;
  if (setsockopt(soquete, SOL_PACKET, PACKET_ADD_MEMBERSHIP, &mr, sizeof(mr)) == -1)	{
    fprintf(stderr, "Erro ao fazer setsockopt\n");
    exit(-1);
  }

  return soquete;
}

/* -- Inicia fila enviando com tamanho n -- */
int go_back_n(int n){
  fprintf(stderr, "INFO: [GO_BACK_N] BEGIN --\n");

  tamanho_fila_enviando = n;
  //cria nova fila com os n elementos corretos da fila para enviar
  int count = 0;
  int size = queue_size((queue_t*)fila_para_enviar);
  fprintf(stderr, "INFO: [GO_BACK_N] Faltam %d na fila de enviar, count: %d, n: %d\n", size, count, n);
  while(count < n && fila_para_enviar != NULL){
    fprintf(stderr, "INFO: [GO_BACK_N] Adicionando elementos na fila, tentaremos %d\n", n);

    package_queue_t* elem = fila_para_enviar;
    queue_remove((queue_t**) &fila_para_enviar, (queue_t*)elem);
    queue_append((queue_t**) &fila_enviando, (queue_t*)elem);
    // queue_append((queue_t**)fila_enviando, (queue_t*)elem);
    count ++;
  }

  tamanho_fila_enviando = queue_size((queue_t*)fila_enviando);;

  if(tamanho_fila_enviando == 0){
    fprintf(stderr, "INFO: [GO_BACK_N] Alterando para modo de recebimento\n");
    mode = RECIEVE_MODE;
    return 1;
  }

  queue_print("Enviando fila toda", (queue_t*)fila_enviando, (void (*)(void *))send_package_print);


  fprintf(stderr, "INFO: [GO_BACK_N] Foram adicionados %d novos elementos\n", count);

  fprintf(stderr, "INFO: [GO_BACK_N] Alterando para modo de espera de resposta\n");
  mode = WAITING_ANSWER_MODE;



  fprintf(stderr, "INFO: [GO_BACK_N] END --\n");
  return 1;
}



int stop_and_wait(package_queue_t ** fila_enviados){
  fprintf(stderr, "INFO: [stop_and_wait] BEGIN --\n");

  go_back_n(1);

  // if(fila_para_enviar != NULL){
  //   if(mode != WAITING_ANSWER_MODE){
  //     package_queue_t * elem = fila_para_enviar;
  //     send_package(elem->pack, fila_enviados);
  //     fprintf(stderr, "INFO [stop_and_wait]: Alterando para modo esperar resposta\n");
  //     mode = WAITING_ANSWER_MODE;
  //     queue_remove((queue_t**) &fila_para_enviar, (queue_t*) elem);
  //   }
  // }

  fprintf(stderr, "INFO: [stop_and_wait] END --\n");
}


int recebe_ack_nack(package_queue_t* pack, package_queue_t ** fila_enviados, package_queue_t ** fila_recebidos){
  fprintf(stderr, "INFO: [recebe_ack_nack] BEGIN -- %d sequence. MODE == %d\n", pack->pack->head->sequence, mode);

  if(mode == WAITING_ANSWER_MODE){
    /*
      Enquanto elemento está na fila,
        libera elemento fila até ele não estar mais
      e depois repõe a fila até 
        n elementos na fila ou até não haver mais elementos a serem enviados
    */


    // while(elem->pack->head->sequence == fila_enviando->pack->head->sequence){
    while(queue_find((queue_t**)&fila_enviando, (queue_t*) pack,  (int (*)(void *, void *)) compare_packs_by_seq)){
      package_queue_t* elem = fila_enviando;
      fprintf(stderr, "INFO: [recebe_ack_nack] Removendo elemento %d --\n", elem->pack->head->sequence);
      queue_remove((queue_t**) &fila_enviando, (queue_t*)elem);
      queue_append((queue_t**)fila_enviados, (queue_t*)elem);
      if(elem->pack->head->type == TEXT_TYPE)
        adiciona_no_historico(0, elem);
    }

    go_back_n(tamanho_fila_enviando);

  }

  fprintf (stderr, "INFO: [recebe_ack_nack] Não é necessário responder para ACK ou NACK\n");
  fprintf(stderr, "INFO: [recebe_ack_nack] END --\n");
  return 1;

}



int enviar_arquivo(char* path, int pathSize, package_queue_t ** fila_enviados){
  if(pathSize <= 0)
    return 0;

  FILE *fp = fopen(path, "r");
  if (fp == NULL){
    fprintf(stderr, "ERR: [enviar] Não pude abrir arquivo\n");
  }

  file_to_package(fp, &fila_para_enviar);

  go_back_n(STD_TAMANHO_JANELA);
}


/* -- Verifica se recebeu um pacote íntegro  -- */
int validate_input(char *msg, int size){
  if (size <= 0) { return 0; }

  header * p = malloc(HEADER_SIZE);
  
  memcpy(p, msg, HEADER_SIZE);

  if(p->init_mark != INIT_MARK){
    // printf("Error in INIT_MARK\n");
    return 0;
  }

  // for(int i = 0; i < size; i++){
  //   printf("%x ", msg[i]);
  // }
  
  fprintf (stderr, "\nINFO: BEGINING validate_input -- \n\n");

  
  fprintf (stderr, "Head: Init[%x], Type[%02x], Seq[%01x], size[%02x]\n", p->init_mark, p->type, p->sequence, p->size );

  int lineSize = p->size;
  char * data = malloc(lineSize);
  
  // printf("Tamanho = %d\n", lineSize);

  memcpy(data, msg + HEADER_SIZE, lineSize);
  // printf("->..%s %d\n", msg + HEADER_SIZE, msg + HEADER_SIZE+lineSize);
  
  fprintf (stderr, "Data: '%s'\n", data);

  uint8_t crc = msg [size-1];
  fprintf (stderr, "Received CRC: %d\n", crc);

  uint8_t calc_crc = crc8(data, p->size);

  fprintf (stderr, "Calculated CRC: %d\n", calc_crc);

  if (crc != calc_crc) { 
    fprintf(stderr, "ERR: Wrong CRC. Expecting %d calculated %d\n", crc, calc_crc);
    return 0;
  }

  fprintf (stderr, "Sucesso \n\n");


  fprintf (stderr, "INFO: END validate_input -- \n\n");

  return 1;

}

/* -- Verifica o que fazer com o pacote lido e faz --  */
int recieve_and_answer( package_queue_t* pack, package_queue_t ** fila_recebidos, package_queue_t ** fila_enviados){
  fprintf(stderr, "INFO: BEGINING recieve_and_answer\n");

  int type = pack->pack->head->type;
  int ja_recebido = 0;
  /* Verifica se é nosso arquivo ou nem. Ocorre quando está usando loopback*/

  fprintf(stderr, "Procurando \n");

  if( (queue_find((queue_t**) fila_enviados, (queue_t*) pack, (int (*)(void *, void *)) compare_packs) != NULL )
      || (queue_find((queue_t**) &fila_enviando, (queue_t*) pack, (int (*)(void *, void *)) compare_packs) != NULL) ){
    fprintf(stderr, "WAR: [recieve_and_answer] Pacote recebido foi encontrado em lista de enviados nem de enviando, é nosso!  \n");
    return 1;
  }else {
    fprintf(stderr, "WAR: [recieve_and_answer] Pacote recebido não foi encontrado em lista de enviados nem de enviando, é de usuário externo\n");
  }

  /* Verifica se pacote já foi recebido corretamente anteriormente, para não duplicar no histórico */

  if( queue_find((queue_t**) fila_recebidos, (queue_t*) pack, (int (*)(void *, void *)) compare_packs) != NULL ){
    fprintf(stderr, "WAR: [recieve_and_answer] pacote já recebido anteriormente\n");
          ja_recebido = 1;
  } else {
    fprintf(stderr, "WAR: [recieve_and_answer] pacote nuunca recebido\n");

    queue_append((queue_t**) fila_recebidos, (queue_t*) pack);

    if(pack->pack->head->type == TEXT_TYPE)
      adiciona_no_historico(1, pack);

  }


  /* -- Caso seja ack ou nack, verifica se estamos esperando resposta e age de acordo-- */
  if(type == ACK_TYPE || type == NACK_TYPE){
    recebe_ack_nack(pack, fila_enviados, fila_recebidos);
    return 1;
  }

  // INI_TRANS_TYPE ocorre no inicio de um arquivo enviado
  if(type == INI_TRANS_TYPE){
    fprintf(stderr, "INFO: [recieve_and_answer] Identificado recebimento de arquivo\n");

    file_recieving = fopen(*pack->pack->data, "w");
    if(!file_recieving){
      fprintf(stderr, "ERR: [recieve_and_answer] Impossivel receber arquivo\n");
      send_nack(pack);
      return 0;
    }

    fprintf(stderr, "INFO: [recieve_and_answer] Alterando modo de recebimento de arquivo\n");
    mode = RECIEVE_FILE_MODE;
  }


  if(type == MIDIA_TYPE){
    fprintf(stderr, "INFO: [recieve_and_answer] Identificado continuação de arquivo\n");
    if(!file_recieving){
      fprintf(stderr, "ERR: [recieve_and_answer] Impossivel escrever no arquivo de recebimento\n");
      send_nack(pack);
      return 0;
    }
    if(!ja_recebido){
      fprintf(stderr, "INFO: [recieve_and_answer] Escrevendi no arquivo\n");
      fprintf(file_recieving, "%s", *pack->pack->data);
    }
  }

  
  if(type == END_TRANS_TYPE){
    fprintf(stderr, "INFO: [recieve_and_answer] Identificado fim de arquivo\n");
    if(!file_recieving){
      fprintf(stderr, "ERR: [recieve_and_answer] Impossivel encontrar o arquivo de recebimento para fechá-lo\n");
      send_nack(pack);
      return 0;
    }
    fprintf(stderr, "INFO: [recieve_and_answer] Alterando modo de recebimento de arquivo\n");
    mode = RECIEVE_MODE;
    if(!ja_recebido){
      fprintf(stderr, "INFO: [recieve_and_answer] Fecharei o arquivo :)\n");
      
      fclose(file_recieving);
      fprintf(stderr, "INFO: [recieve_and_answer] Alterando modo para recebimento\n");

      adiciona_no_historico(1, pack);
    }
  }




  send_ack(pack);
  
  fprintf(stderr, "INFO: [recieve_and_answer] END --");

  return 1;
}

int inicia_tela(){
	int nlin, ncol;

	initscr();
  	getmaxyx(stdscr, nlin, ncol);

  	if(nlin < 37 || ncol < 100){
  		endwin();
  		printf("o terminal deve ter no mínimo 37 linhas por 100 colunas %d %d\n",nlin, ncol);
  		return 0;
  	}

    cbreak();               /* desabilita o buffer de entrada */
    noecho();               /* não mostra os caracteres digitados */
    nodelay(stdscr, TRUE);  /* faz com que getch não aguarde a digitação */
    keypad(stdscr, TRUE);   /* permite a leitura das setas */
    curs_set(FALSE);        /* não mostra o cursor na tela */

  	return 1;
}

int recebe(package_queue_t ** fila_recebidos, package_queue_t ** fila_enviados){
  char buf[67];
  memset(buf, 0, sizeof(buf));
  int r = recv(soquete , &buf, sizeof(buf), 0);
  // fprintf(stderr, "RECEBIDO NOVO PACOTE\n");
  if (r == -1) {
    // fprintf(stderr, "Erro no recv");
    return 0;
    //exit(-1);
  }

  
  if ( validate_input(buf, r) ) {
    package_queue_t * p = char_to_package(buf, r);
    recieve_and_answer(p, fila_recebidos, fila_enviados);
  } else {
    //fprintf(stderr, "ERR: Package invalid\n");
  }
}

int create_user(char** username){
  unsigned char *msg;
  size_t len = 0;
  ssize_t nameSize = 0;

  do{
    printf("Insira seu nome de usuário:\n");
    nameSize = getline(username, &len, stdin);

    if(nameSize < 2) printf("Seu nome precisa ter no mínimo uma letra!\n");
  } while (nameSize < 2);

  (*username)[nameSize-1] = '\0';

  return 1;
}

int main(int argc, char const *argv[])
{
  char* username;
  create_user(&username);

	if(!inicia_tela())
		return 0;
	
 
  soquete = cria_raw_socket("lo");
  fprintf(stderr, "INFO: [cria_raw_socket] Socket %d created successfully\n", soquete);

  tamanho_fila_enviando = 0;
  mode = RECIEVE_MODE;
  int key;

  package_queue_t ** fila_recebidos = malloc(sizeof(package_queue_t*));
  *fila_recebidos = NULL;
  package_queue_t ** fila_enviados = malloc(sizeof(package_queue_t*));
  *fila_enviados = NULL;

  fila_historico = malloc(sizeof(display_messages_queue_t*));
  *fila_historico = NULL;

  char *line = malloc(80 * sizeof(char));
  size_t lineSize = 0;
  while (1){
    key = getch();
    
    switch (mode) {
    case RECIEVE_MODE:
      if( key == 'q'){
        endwin();
        exit(0);  
      } else if(key == 'i'){
        mode = WRITE_MODE;
        fprintf(stderr, "INFO [display]: Alterando para modo inserção\n");
      } else if(key == 'a'){
        mode = FILE_MODE;
        fprintf(stderr, "INFO [display]: Alterando para modo arquivo\n");
      }
      break;
    
    case WRITE_MODE:
      if(key == '\n'){
        mode = RECIEVE_MODE;
        fprintf(stderr, "INFO [display]: Alterando para modo receber\n");
        
        line_to_package(line, lineSize, &fila_para_enviar);
        stop_and_wait(fila_enviados);
        
        line = malloc(MAX_SIZE_OF_INPUT * sizeof(char));
        lineSize = 0;
      } else if (key == KEY_BACKSPACE && lineSize > 0 && lineSize < MAX_SIZE_OF_INPUT) {
        lineSize--;
        line[lineSize] = '\0';
      }else if(key != -1){
        line[lineSize] = key;
        line[lineSize+1] = '\0';
        lineSize++;
      } 
      break;
    
    case FILE_MODE:
        if(key == '\n'){
        mode = RECIEVE_MODE;
        fprintf(stderr, "INFO [display]: Alterando para modo receber\n");

        enviar_arquivo(line, lineSize, fila_enviados);        
        
        line = malloc(80 * sizeof(char));
        lineSize = 0;
      } else if (key == KEY_BACKSPACE && lineSize > 0) {
        lineSize--;
        line[lineSize] = '\0';
      }else if(key != -1){
        line[lineSize] = key;
        line[lineSize+1] = '\0';
        lineSize++;
      } 
      break;
    
    case WAITING_ANSWER_MODE:

      break;
    default:
      break;
    }


    if(mode == RECIEVE_MODE || mode == WAITING_ANSWER_MODE || mode == RECIEVE_FILE_MODE){
      recebe(fila_recebidos, fila_enviados);
    } else {
      
    }

    erase();
    desenha_tabuleiro(mode, username);
    desenha_msgs(fila_historico, username);

    if(mode == WRITE_MODE || mode == FILE_MODE){
      desenha_texto(line);
    }

		refresh();
		usleep(33500);
  }


  return 0;
}
